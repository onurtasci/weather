import React from 'react'
import { Image } from 'react-native'
import styled from 'styled-components/native'

const CustomIcon = (props) => {

    const width = props.size ? props.size : 50

    const iconObject = icons.find(ic => ic.name === props.name)

    src = props.url ? { uri: props.url } : iconObject?.link

    const Container = styled.View`
        padding: 0 10px;
    `
    return (
        <Container>
            {/* Aqui Inline-styling usado para configurar o tamanho da imagem do componente pai */}
            <Image style={{ width: width, height: width, resizeMode: 'cover' }} source={src} />
        </Container>
    )
}

const icons = [
    { name: 'sunny', link: require('../../assets/icons/sunny.png') },
    { name: 'cloudy', link: require('../../assets/icons/cloudy.png') },
    { name: 'rainy', link: require('../../assets/icons/rainy.png') },
    { name: 'location', link: require('../../assets/icons/location.png') },
    { name: 'empty', link: require('../../assets/icons/empty.png') },
    { name: 'humidity', link: require('../../assets/icons/humidity.png') },
    { name: 'pressure', link: require('../../assets/icons/pressure.png') },

]

export default CustomIcon