import React from 'react'
import { ActivityIndicator } from 'react-native'
import CustomIcon from '../UI/CustomIcon'
import styled from 'styled-components/native'

const DetailCard = props => {

    const data = props.data

    const Container = styled.View`
        width: 94%;
        height: 100px;
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
        background-color: rgba(255,255,255, 0.7);
        margin: 20px;
        padding: 20px;
        border-radius: 8px;
    `
    const LeftContainer = styled.View`
        width: 50%;
        display: flex;
        align-items: center;
        justify-content: center;
    `
    const VerticalLine = styled.View`
        height: 100%;
        border: 1px solid white;
    `
    const RightContainer = styled.View`
        width: 50%;
        display: flex;
        align-items: center;
        justify-content: center;
    `
    const Label = styled.Text`
        color: rgb(56,60,73);
    `
    const Value = styled.Text`
        color: rgb(56,60,73);
        font-weight: bold;
    `

    const Empty = styled.View`
        height: 100px;
    `

    if (!data) {
        return <Empty/>
    }

    return (
        <Container>
            <LeftContainer>
                <CustomIcon name='humidity' size={25} />
                <Label>Umidade</Label>
                <Value>{data?.main.humidity}</Value>
            </LeftContainer>
            <VerticalLine />
            <RightContainer>
                <CustomIcon name='pressure' size={25} />
                <Label>Pressão</Label>
                <Value>{data?.main.pressure}</Value>
            </RightContainer>
        </Container>
    )
}

export default DetailCard