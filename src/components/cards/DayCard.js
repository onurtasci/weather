import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import CustomIcon from '../UI/CustomIcon'
import styled from 'styled-components/native'

const deviceWidth = Dimensions.get('window').width

const DayCard = props => {
    const [iconName, setIconName] = useState(null)

    const item = props.item

    let tempCelcius = ((5 / 9) * (item.data?.main.temp - 32)).toFixed(0);
    let maxTemp = ((5 / 9) * (item.data?.main.temp_max - 32)).toFixed(0);
    let minTemp = ((5 / 9) * (item.data?.main.temp_min - 32)).toFixed(0);
    let feelsLike = ((5 / 9) * (item.data?.main.feels_like - 32)).toFixed(0);

    useEffect(() => {
        if (item.data?.weather[0].description.includes('sun'))
            setIconName('sunny')

        if (item.data?.weather[0].description.includes('clear'))
            setIconName('sunny')

        if (item.data?.weather[0].description.includes('cloud'))
            setIconName('cloudy')

        if (item.data?.weather[0].description.includes('rain'))
            setIconName('rainy')
    }, [item])

    const SubContainer = styled.View`
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
        margin: 0 5px;
    `
    
    const CityText = styled.Text`
        font-size: 12px;
        color: rgb(56,60,73);
    `
    const DegreeText = styled.Text`
        font-size: 15px;
        color: rgb(56,60,73);
    `
    const DateText = styled.Text`
        font-size: 12px;
        text-align: center;
        color: rgb(56,60,73);
        position: absolute;
        top: 10px;
    `
    const NoDataText = styled.Text`
        font-size: 12px;
        text-align: center;
        margin-top: 10px;
        color: rgb(56,60,73);
    `

    if (!item?.data) {
        return (
            <View style={styles.container}>
                <DateText>{item?.date}</DateText>
                <CustomIcon name='empty' size={30} />
                <NoDataText>Nenhum dado encontrado para esta data</NoDataText>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <DateText>{item?.date}</DateText>
            <CityText>{item.data.name}</CityText>
            <SubContainer>
                <CustomIcon name={iconName} size={30} />
                <DegreeText>{tempCelcius}°</DegreeText>
            </SubContainer>
            <CityText>{maxTemp}°/ {minTemp}°</CityText>
            <CityText>Sentida {feelsLike}°</CityText>
        </View>
    )
}

// Aqui, para poder usar a largura do dispositivo, tive que usar StyleSheet em vez de StyledComponents

const styles = StyleSheet.create({
    container: {
        width: (deviceWidth - 60) / 3,
        height: 150,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginHorizontal: 10,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 8,
        backgroundColor: 'rgba(255,255,255, 0.7)',
    },
})

export default DayCard
