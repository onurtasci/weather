import React, { useState, useEffect } from 'react'
import { ActivityIndicator } from 'react-native'
import CustomIcon from '../UI/CustomIcon'
import styled from 'styled-components/native'
import moment from 'moment'

const today = moment().format('DD MMMM YYYY')

const TodaysCard = props => {
    const [iconName, setIconName] = useState(null)

    let tempCelcius = ((5 / 9) * (props.data?.main.temp - 32)).toFixed(0);
    let maxTemp = ((5 / 9) * (props.data?.main.temp_max - 32)).toFixed(0);
    let minTemp = ((5 / 9) * (props.data?.main.temp_min - 32)).toFixed(0);
    let feelsLike = ((5 / 9) * (props.data?.main.feels_like - 32)).toFixed(0);

    const { data } = props
    useEffect(() => {
        if (data?.weather[0].description.includes('sun'))
            setIconName('sunny')

        if (data?.weather[0].description.includes('clear'))
            setIconName('sunny')

        if (data?.weather[0].description.includes('cloud'))
            setIconName('cloudy')

        if (data?.weather[0].description.includes('rain'))
            setIconName('rainy')
    }, [data])

    const Container = styled.View`
        height: 250px;
        display: flex;
        align-items: center;
        justify-content: center;
    `
    const SubContainer = styled.View`
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: center;
    `
    const CityText = styled.Text`
        font-size: 20px;
        color: white;
        font-weight: bold;
    `
    const DateText = styled.Text`
        font-size: 13px;
        color: #ccc;
        margin-bottom: 20px;
    `
    const DegreeText = styled.Text`
        font-size: 60px;
        color: white;
    `
    const InfoText = styled.Text`
        font-size: 15px;
        color: white;
    `

    if (!props.data) {
        return (
            <Container>
                <ActivityIndicator color='white' size='large' />
            </Container>
        )
    }

    return (
        <Container>
            <SubContainer>
                <CustomIcon name='location' size={20} />
                <CityText>{props.data?.name}, {props.data?.sys.country}</CityText>
            </SubContainer>
            <DateText>{today}</DateText>
            <SubContainer>
                <CustomIcon name={iconName} />
                <DegreeText>{tempCelcius}°</DegreeText>
            </SubContainer>
            <InfoText>{maxTemp}°/ {minTemp}° Temperatura Sentida {feelsLike}°</InfoText>
            <InfoText>{props.data?.weather[0].description}</InfoText>
        </Container>
    )
}

export default TodaysCard