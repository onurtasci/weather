import React, { useEffect, useState } from 'react';
import { StatusBar, FlatList } from 'react-native';
import TodaysCard from './src/components/cards/TodaysCard';
import axios from 'axios'
import Geolocation from '@react-native-community/geolocation';
import DayCard from './src/components/cards/DayCard';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import DetailCard from './src/components/cards/DetailCard';
import styled from 'styled-components/native'

const apiKey = 'a6f7e9871083d6ed32138c0666292c43'
const today = moment().format('DD/MM/YYYY')

const App = () => {
  const [fetchingCurrent, setFechingCurrent] = useState(false)
  const [data, setData] = useState(null)
  const [pastData, setPastData] = useState([])

  useEffect(() => {
    fetchWeather()
  }, [])

  const fetchWeather = () => {
    Geolocation.getCurrentPosition(info => {
      setFechingCurrent(true)
      axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${info.coords.latitude}&lon=${info.coords.longitude}&units=imperial&APPID=${apiKey}`)
        .then(async (response) => {
          setData(response.data)
          const past = await getPastData()
          let newList = []
          if (past?.length > 0) {
            newList = past
            if (past[0].date !== today) {
              newList.unshift({ data: response.data, date: today })
            }
            if (past.length > 8) {
              newList.pop()
            }
            await AsyncStorage.setItem('pastData', JSON.stringify(newList))
            setPastData(newList.filter((item, index) => index !== 0))
          } else {
            for (let i = 1; i <= 8; i++) {
              const relatedDate = moment().subtract(i - 1, 'd').format('DD/MM/YYYY');
              newList.push({ data: i === 1 ? response.data : null, date: relatedDate })
            }
            await AsyncStorage.setItem('pastData', JSON.stringify(newList))
            setPastData(newList.filter((item, index) => index !== 0))
          }
          setFechingCurrent(false)
        })
        .catch(err => {
          console.log(err)
          setFechingCurrent(false)
        })
    })
  }

  const getPastData = async () => {
    try {
      const resData = await AsyncStorage.getItem("pastData");
      if (resData) {
        let transformedData = JSON.parse(resData);
        return transformedData
      } else {
        return null
      }
    } catch (err) {
      throw new Error(err);
    }
  };

  const Screen = styled.SafeAreaView`
    display: flex;
    width: 100%;
    height: 100%;
    background-color: rgb(12,139,166);
  `
  const Scroll = styled.SafeAreaView`
    display: flex;
    width: 100%;
    height: 100%;
    align-items: center;
    justify-content: space-around;
  `
  const Background = styled.ImageBackground`
    display: flex;
    width: 100%;
    height: 100%; 
    justify-content: center;
  `
  const ListContainer = styled.View`
    width: 100%;
    height: 200px;
  `
  const Title = styled.Text`
    color: white;
    text-align: center;
    font-size: 18px;
    margin-bottom: 10px;
  `

  return (
    <Screen>
      <StatusBar barStyle='light-content'/>
      <Background source={require('./src/assets/images/background.jpeg')} resizeMode="cover">
      <Scroll>
        <TodaysCard data={data} />
        <DetailCard data={data} hide={fetchingCurrent}/>
        <ListContainer>
          {pastData?.length > 0 && <Title>Últimos 7 Dias</Title>}
          <FlatList
            horizontal={true}
            contentContainerStyle={{ height: 150 }}
            keyExtractor={(item, index) => index.toString()}
            data={pastData}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }) => (<DayCard item={item} />)}
          />
        </ListContainer>
      </Scroll>
      </Background>
    </Screen>
  );
};

export default App;
